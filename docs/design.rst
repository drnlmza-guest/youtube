Design
======

Data is stored in YAML, in git, for ease of contribution and
collaboration.

This is just a simple set of scripts for manipulating this YAML
metadata.

The tools:

``merge-meta``
--------------

Create/Update a YouTube metadata file for each set of videos in the
``archive-meta`` repo.
These will ultimately become YouTube playlists.

``auth``
--------

Authenticate to YouTube, and do nothing else. Use this tool to create /
verify upload credentials.

``upload_youtube``
------------------

Upload all videos in a metadata file to YouTube, setting metadata
appropriately.

Videos that have a ``youtube_id`` attribute are already uploaded, and
skipped.

Produce a log file of completed upload video IDs.

``merge-uploads``
-----------------

Merge the video IDs from ``upload_youtube`` into a YouTube metadata
file, setting ``youtube_id`` attributes.

``publish-slowly``
------------------

Slowly work through the videos in a YouTube metadata file, publishing 1
previously unlisted video, a day.
This is friendlier to the YouTube channel subscribers, than huge batches
of video.
