#!/usr/bin/env python3
# coding: utf8

import argparse
from pathlib import Path

import yaml


def index(list_, key='file'):
    by_fn = {}
    for video in list_:
        fn = video[key]
        by_fn[fn] = video
    return by_fn


def merge(meta, yt_meta):
    """Merge videos from conference into our existing YouTube metadata."""
    conference = meta['conference']
    meta['videos'] = [video for video in meta['videos']
                      if 'non-free' not in video]
    meta_index = index(meta['videos'], 'video')
    orphans_index = index(yt_meta.get('orphans', []))
    yt_index = index(yt_meta.setdefault('videos', []))

    # Find orphans and archive them
    for fn in set(yt_index) - set(meta_index):
        video = yt_index.pop(fn)
        yt_meta['videos'].remove(video)
        orphans_index[fn] = video
        yt_meta.setdefault('orphans', []).append(video)

    # Add missing videos
    for fn in set(meta_index) - set(yt_index):
        video = {'file': fn}
        yt_meta['videos'].append(video)
        yt_index[fn] = video

    # Order the YT meta list like meta
    yt_meta['videos'] = [yt_index[video['video']] for video in meta['videos']]

    # Merge metadata from meta into yt_meta, for each video
    for video in yt_meta['videos']:
        meta_video = meta_index[video['file']]
        update_video(meta_video, video, conference)


def update_video(meta_video, video, conference):
    """Update video with details from meta_video"""
    title = youtube_escape(meta_video['title'])
    video['title'] = truncate(title, 100)

    description = []
    if video['title'] != title:
        description.append(title)
    if meta_video.get('speakers'):
        for speaker in meta_video['speakers']:
            description.append('by {}'.format(speaker))
        description.append('')

    if conference.get('title'):
        description.append(
            'At: {}'.format(conference['title']))
    if meta_video.get('details_url'):
        description.append(meta_video['details_url'])
        if (conference.get('website') and not
                meta_video['details_url'].startswith(conference['website'])):
            description.append(conference['website'])
    elif conference.get('website'):
        description.append(conference['website'])

    if meta_video.get('description'):
        description.append('')
        description.append(meta_video['description'])
        description.append('')

    if meta_video.get('room'):
        description.append('Room: {}'.format(meta_video['room']))
    if meta_video.get('start'):
        description.append(
            'Scheduled start: {}'.format(meta_video['start']))
    description = youtube_escape('\n'.join(description))
    video['description'] = truncate(description, 5000)


def youtube_escape(string):
    """<> are not permitted. Apply appropriate workarounds"""
    # Was it an ASCII art arrow?
    string = string.replace(' -> ', ' → ')
    string = string.replace(' <- ', ' ← ')
    # Visually similar but legal
    string = string.replace('<', '❮')
    string = string.replace('>', '❯')
    return string


def truncate(string, length):
    """YouTube has length limits for things like titles"""
    if len(string) <= length:
        return string
    string = string[:length-1] + '…'
    return string


def main():
    parser = argparse.ArgumentParser(
        description='Merge metadata from the archive-meta repo into here')
    parser.add_argument('archive-meta', metavar='LOCATION',
                        help='Location of the archive-meta metadata')
    args = parser.parse_args()

    base = Path(getattr(args, 'archive-meta'))
    for conf_meta_path in base.glob('*/*.yml'):
        relative_path = conf_meta_path.relative_to(base)
        print('Merging ', relative_path)
        with conf_meta_path.open() as f:
            conf_meta = yaml.safe_load(f)

        video_path = Path('metadata') / relative_path

        video_meta = {}
        if video_path.exists():
            with video_path.open() as f:
                video_meta = yaml.safe_load(f)

        merge(conf_meta, video_meta)

        if not video_meta['videos']:
            if video_path.exists():
                video_path.unlink()
            continue

        video_path.parent.mkdir(exist_ok=True)

        with video_path.open('w') as f:
            f.write('---\n')
            yaml.safe_dump(
                video_meta, f, allow_unicode=True, default_flow_style=False)


if __name__ == '__main__':
    main()
