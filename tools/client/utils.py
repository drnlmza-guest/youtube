def iter_pages(query_func, **kwargs):
    pageToken = None
    while True:

        r = query_func(
            pageToken=pageToken,
            **kwargs
        ).execute()

        for item in r['items']:
            yield item

        if 'nextPageToken' not in r:
            return

        pageToken = r['nextPageToken']
