from client.utils import iter_pages
from client.video import Video
from client.youtube import YouTube


class Playlist:
    def __init__(self, data):
        self.data = data
        self._yt = YouTube.client()

    @property
    def id(self):
        return self.data['id']

    @classmethod
    def create(cls, title, description='', privacy='public',
               default_language='en-us', tags=()):
        youtube = YouTube.client()
        body = {
            'snippet': {
                'title': title,
                'description': description,
                'defaultLanguage': default_language,
                'tags': tags,
            },
            'status': {
                'privacyStatus': privacy,
            }
        }
        r = youtube.playlists().insert(
            part='snippet,status', body=body).execute()
        return Playlist(data=r)

    @classmethod
    def filter(cls, channel_id=None, ids=None):
        youtube = YouTube.client()
        filters = {}
        if channel_id and ids:
            raise ValueError('only one of channel_id and ids may be specified')
        if channel_id:
            filters['channelId'] = channel_id
        elif ids:
            filters['id'] = ','.join(ids)
        else:
            filters['mine'] = True

        for item in iter_pages(
                youtube.playlists().list,
                part='snippet,contentDetails', **filters):
            yield Playlist(data=item)

    @classmethod
    def get_by_id(cls, id):
        youtube = YouTube.client()
        filters = {
            'id': id,
        }

        r = youtube.playlists().list(
            part='snippet,contentDetails', **filters).execute()

        assert len(r['items']) == 1
        return Playlist(data=r['items'][0])

    def delete(self):
        self._yt.playlists().delete(id=self.id).execute()

    def list_items(self):
        for item in iter_pages(
                self._yt.playlistItems().list,
                maxResults=50,
                part='snippet,contentDetails',
                playlistId=self.id):
            yield PlaylistItem(item)

    def insert(self, video, position=None):
        video_id = video
        if isinstance(video, Video):
            video_id = video.id

        body = {
            'snippet': {
                'playlistId': self.id,
                'resourceId': {
                    'kind': 'youtube#video',
                    'videoId': video_id,
                },
            },
        }
        if position is not None:
            body['snippet']['position'] = position

        r = self._yt.playlistItems().insert(
            body=body, part='snippet').execute()
        return PlaylistItem(r)


class PlaylistItem:
    def __init__(self, data):
        self.data = data
        self._yt = YouTube.client()

    @property
    def id(self):
        return self.data['id']

    @property
    def video_id(self):
        return self.data['contentDetails']['videoId']

    def get_video(self):
        return Video.get_by_id(self.video_id)

    def reposition(self, position):
        r = self._yt.playlistItems().update(
            part='snippet',
            body={
                'id': self.id,
                'snippet.playlistId': self.data['snippet']['playlistId'],
                'snippet.position': position,
            }
        ).execute()
        self.data['snippet'] = r['snippet']

    def delete(self):
        self._yt.playlistItems().delete(id=self.id).execute()
