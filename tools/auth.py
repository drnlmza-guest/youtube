#!/usr/bin/env python3

import argparse
import logging

from client.youtube import YouTube



def main():
    p = argparse.ArgumentParser()
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity.')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)
    logging.getLogger('googleapiclient').setLevel(logging.ERROR)
    YouTube.client()


if __name__ == '__main__':
    main()
